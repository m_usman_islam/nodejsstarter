require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');

const app = express();
const userRoutes = require('./routes/user');

app.use(express.json());

// import routes

app.use('/api/v1/user', userRoutes);

mongoose
  .connect(process.env.URL)
  .then(() => {
    app.listen(process.env.PORT, () => {
      console.log('yahooo server & db is on now');
    });
  })
  .catch((e) => {
    console.log('error', e);
  });
