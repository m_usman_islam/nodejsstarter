const User = require('../models/userScheme');

// find all user
const getAllUsers = async (req, res) => {
  try {
    let response = await User.find({});
    res.json(response);
  } catch (error) {
    res.json({ error });
  }
};

// find a single user
const getSindleUsers = async (req, res) => {
  try {
    let response = await User.findById({ _id: req.params.id });
    res.json(response);
  } catch (error) {
    res.json({ error });
  }
};

// create new user
const createNewUser = async (req, res) => {
  let { name, age, address, email } = req.body;

  try {
    let response = await User.create({ name, age, address, email });

    res.json(response);
  } catch (error) {
    res.json({ error });
  }
};
//update  user
const updateUser = async (req, res) => {
  try {
    let response = await User.findByIdAndUpdate(
      { _id: req.params.id },
      { ...req.body }
    );

    res.json(response);
  } catch (error) {
    res.json({ error });
  }
};

//remove user
const deleteUser = async (req, res) => {
  try {
    let response = await User.remove({ _id: req.params.id });

    res.json(response);
  } catch (error) {
    res.json({ error });
  }
};

module.exports = {
  getAllUsers,
  getSindleUsers,
  createNewUser,
  updateUser,
  deleteUser,
};
