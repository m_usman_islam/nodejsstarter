const mongoose = require('mongoose');
const userScheme = mongoose.Schema(
  {
    name: {
      type: String,
      requried: true,
    },
    age: {
      type: Number,
      requried: true,
    },
    address: {
      type: String,
      requried: true,
    },
    email: {
      type: String,
      requried: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('User', userScheme);
