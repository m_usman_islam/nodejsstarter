const express = require('express');

const router = express.Router();

//import controller
const {
  getAllUsers,
  getSindleUsers,
  createNewUser,
  updateUser,
  deleteUser,
} = require('../controller/userController');

// find all user
router.get('/', getAllUsers);

// find a single user
router.get('/:id', getSindleUsers);

// create new user
router.post('/', createNewUser);

//update  user
router.patch('/:id', updateUser);

//remove user
router.delete('/:id', deleteUser);

module.exports = router;
